// coupon/index.js
var common = require('../utils/common');
var server = require('../utils/server');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeNames: ['0'],
    list: [],
    isGetCoupon:false,
    isLogin:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.authLogin = this.selectComponent('#authLogin');

  },
  login(){
    this.authLogin.showLoginPop();

  },
  loginEvent() {
    this.setData({
      isLogin:true
    })
 this.getCouponList();
  },
  regEvent() { },
  onChange(event) {
    console.log(event)
    this.setData({
      activeNames: event.detail,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.checkLogin()
  },
  checkLogin() {
    const userInfo = wx.getStorageSync('token');
    common.checkToken(userInfo).then(res => {
      if (res.status != 1) {
        this.setData({
          isLogin: false,
        });
      } else {
        this.setData({
          isLogin: true,
        });
        this.getCouponList()
      }
    })
  },
  saveCoupon(e) {
    var id = e.currentTarget.dataset.id,
    index=e.currentTarget.dataset.index;
    if(this.data.isLogin){
      this.setData({
        isGetCoupon:true
      })
      server.postJSON(common.basePath + 'coupon/save/' + id, "", res => {
        this.setData({
          isGetCoupon:false
        })
        if(res.data.successful){
          this.setData({
            ['list[' + index + '].own']: 1
          })
        }else{
          common.showErrorToast(res.data.msg)
        }
       
      })
    }else{
      this.authLogin.showLoginPop();
    }
   
  },
  goToIndex() {
    wx.switchTab({
      url: '../pages/index/index',
    })
  },
  getCouponList() {
    server.getJSON(common.basePath + 'coupon/list', "", res => {
      this.setData({
        list: res.data.data.list
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})