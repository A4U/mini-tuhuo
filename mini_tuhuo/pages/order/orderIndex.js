var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({
  data: {
    lists: [],
    currentTab: 1,
    isEmpty: false,
    reasonItems: [{
      "id": 1,
      name: "不想要了"
    }, {
      "id": 2,
      name: "发货太慢"
    }, {
      "id": 5,
      name: "其他"
    }],
    classHeight: "",
    scrollnum: 0,
    page: 1,
    isFirstSearch: true,
    searchLoading: true, //"上拉加载"的变量，默认false，隐藏
    searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏,
    showReasonPop: false,
    showLoginPop: false
  },
  onLoad: function(options) {
    wx.createSelectorQuery().select('.tab-cont').boundingClientRect(res => {
      console.log(res.bottom)
      this.setData({
        classHeight: wx.getSystemInfoSync().windowHeight - res.bottom
      })
      console.log(this.data.classHeight)
    }).exec()
  },
  onShow: function() {
    this.checkLogin();
  },
  //登录
  login(e) {
    if (e.detail.errMsg == "getUserInfo:ok") {
      this.setData({
        showLoginPop: false
      })
      this.checkLogin();
    } else {
      wx.showToast({
        icon: 'none',
        title: '需要授权用户信息使用土货来了！'
      })
    }
  },
  checkLogin() {
    var that = this;
    wx.login({
      success: resp => {
        wx.getUserInfo({
          success: res => {
            this.setData({
              showLoginPop: false
            })
            console.log(res)
            var code = resp.code,
              userParam = {
                "gender": res.userInfo.gender,
                "avatar": res.userInfo.avatarUrl,
                "nickName": res.userInfo.nickName,
                "recommendId": app.globalData.friendId
              };
            wx.getStorage({
              key: 'token',
              success: res => {
                common.checkToken(res.data).then(res => {
                  if (res.status == 1) {
                    this.setData({
                      page: 1,
                      isFirstSearch: true,
                      searchLoading: true, //"上拉加载"的变量，默认false，隐藏
                      searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
                      scrollnum: 0
                    })
                    this.searchByStatus();
                  } else {
                    this.reLogin(userParam, code)
                  }
                })
              },
              fail: res => {
                console.log(121)
                this.reLogin(userParam, code)
              }
            })
          },
          fail: res => {
            this.setData({
              showLoginPop: true
            })
          }
        })
      }
    })
  },
  reLogin(userParam, code) {
    common.login(userParam, code).then(res => {
      wx.setStorage({
        key: 'userInfo',
        data: res.user
      })
      wx.setStorage({
        key: 'token',
        data: res.user.token,
        success: res => {
          this.setData({
            page: 1,
            isFirstSearch: true,
            searchLoading: true, //"上拉加载"的变量，默认false，隐藏
            searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
            scrollnum: 0
          })
          this.searchByStatus();
        }
      })
    })
  },
  switchNav(e) {
    var orderStatusId = e.currentTarget.dataset.current;
    this.setData({
      currentTab: orderStatusId,
      page: 1,
      isFirstSearch: true,
      searchLoading: true, //"上拉加载"的变量，默认false，隐藏
      searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
      scrollnum: 0
    })
    this.searchByStatus();
  },
  searchByStatus() {
    common.showLoading("加载中...");
    common.hideLoading();
    server.getJSON(common.basePath + 'order/list', {
      "page": this.data.page,
      "pageSize": 10,
      "status": this.data.currentTab
    }, res => {
      if (res.data.successful) {
        if (res.data.data.orderList.length > 0) {
          this.setData({
            lists: res.data.data.orderList,
            isEmpty: false
          })
          if (res.data.data.orderList.length < 10) {
            this.setData({
              searchLoadingComplete: true, //把“没有数据”设为true，显示
              searchLoading: false //把"上拉加载"的变量设为false，隐藏
            });
          }
        } else {
          this.setData({
            isEmpty: true,
            height: '100%'
          })
        }
      } else {
        common.showErrorToast(res.data.errmsg);
      }
    })

  },
  toLower() {
    console.log(3)
    console.log(this.data.searchLoading)
    console.log(this.data.searchLoadingComplete)
    if (this.data.searchLoading && !this.data.searchLoadingComplete) {
      this.setData({
        page: this.data.page + 1, //每次触发上拉事件，把searchPageNum+1
        isFirstSearch: false //触发到上拉事件，把isFromSearch设为为false
      });
      server.getJSON(common.basePath + 'order/list', {
        "page": this.data.page,
        "pageSize": 10,
        "status": this.data.currentTab
      }, res => {
        if (res.data.successful) {
          if (res.data.data.orderList.length != 0) {
            let searchList = [];
            //如果isFromSearch是true从data中取出数据，否则先从原来的数据继续添加
            this.data.isFirstSearch ? searchList = res.data.data.orderList : searchList = this.data.lists.concat(res.data.data.orderList)
            this.setData({
              lists: searchList, //获取数据数组
              searchLoading: true //把"上拉加载"的变量设为false，显示
            });
            //没有数据了，把“没有数据”显示，把“上拉加载”隐藏
          } else {
            this.setData({
              searchLoadingComplete: true, //把“没有数据”设为true，显示
              searchLoading: false //把"上拉加载"的变量设为false，隐藏
            });
          }
        } else {
          common.showErrorToast(res.data.errmsg);
        }
      })
    }
  },
  pay(e) {
    common.payOrder(e.currentTarget.dataset.orderid)
  },
  goToDetail(e) {
    wx.navigateTo({
      url: '../../order/pages/orderDetail?orderid=' + e.currentTarget.dataset.id,
    })
  }

  //取消订单
  // cancelOrder() {
  //   this.setData({
  //     showReasonPop: true
  //   })
  // },
  // confirmCancel() {
  //   this.setData({
  //     showReasonPop: false
  //   })
  // },
  // hidePop() {
  //   this.setData({
  //     showReasonPop: false
  //   })
  // }
})