//index.js
//获取应用实例
const app = getApp();
var server = require('../../utils/server');
var common = require('../../utils/common');
Page({
  data: {
    isLogin: true,
    currentTab: "",
    classHeight: "",
    scrollnum: 0,
    lists: [],
    categoryLists: [],
    page: 1,
    isFirstSearch: true,
    searchLoading: true, //"上拉加载"的变量，默认false，隐藏
    searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
    cartNum: "",
    adWords: "",
    isClickRedBag:false,
    timestamp:(Date.parse(new Date()))
  },
  onReady() {
  },
  loginEvent() {
    this.data.isLogin=true;
    this.getCartNum()
  },
  regEvent() { },
  onLoad: function () {
    this.authLogin = this.selectComponent('#authLogin');
    wx.createSelectorQuery().select('.tab-cont').boundingClientRect(res => {
      console.log(res.bottom)
      this.setData({
        classHeight: wx.getSystemInfoSync().windowHeight - res.bottom,
        topHeight: res.bottom
      })
      console.log(this.data.classHeight)
    }).exec()
  },
  onShow() {
    this.setData({
      scrollleft: 0,
      isClickRedBag:app.globalData.isClickRedBag
    })
    this.getCategoryList();
    this.checkLogin();

  },
  getCartNum() {
    server.getJSON(common.basePath + 'cart', "", res => {
      if (res.data.successful) {
        this.setData({
          cartNum: res.data.data.cart
        })
      } else {
        common.showErrorToast(res.data.errmsg);

      }
    })
  },
  getCategoryList() {
    server.getJSON(common.basePath + 'speciality/category', "", res => {
      if (res.data.successful) {
        this.setData({
          categoryLists: res.data.data.categoryList,
          currentTab: res.data.data.categoryList[0].category,
          adWords: res.data.data.advert
        })
        this.setData({
          scrollnum: 0,
          page: 1,
          isFirstSearch: true,
          searchLoading: true, //"上拉加载"的变量，默认false，隐藏
          searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
        })
        this.getLists(this.data.currentTab);
      } else {
        common.showErrorToast(res.data.errmsg);

      }

    })
  },
  //登录
  login(e) {
    if (e.detail.errMsg == "getUserInfo:ok") {
      this.setData({
        showLoginPop: false
      })
      this.checkLogin();
    } else {
      wx.showToast({
        icon: 'none',
        title: '需要授权用户信息使用土货来了！'
      })
    }
  },
  checkLogin() {
    const userInfo = wx.getStorageSync('token');
    common.checkToken(userInfo).then(res => {
      if (res.status != 1) {
        this.setData({
          isLogin: false,
        });
      } else {
        this.setData({
          isLogin: true,
        });
        this.getCartNum()
      }
    })
  },
  getLists() {
    server.getJSON(common.basePath + 'speciality/list', {
      "attribution": "",
      "category": this.data.currentTab,
      "page": this.data.page,
      "pageSize": 10
    }, res => {
      if (res.data.successful) {
        wx.stopPullDownRefresh();
        this.setData({
          lists: res.data.data.specialityList
        })
        if (res.data.data.specialityList.length < 10) {
          this.setData({
            searchLoadingComplete: true, //把“没有数据”设为true，显示
            searchLoading: false //把"上拉加载"的变量设为false，隐藏
          });
        }
        console.log(this.data.lists)
      } else {
        common.showErrorToast(res.data.data.errmsg)
      }

    })
  },
  switchTab(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.tabid,
      page: 1,
      isFirstSearch: true,
      searchLoading: true, //"上拉加载"的变量，默认false，隐藏
      searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
      scrollnum: 0
    })
    this.getLists(e.currentTarget.dataset.tabid)
  },
  toLower() {
    console.log(3)
    if (this.data.searchLoading && !this.data.searchLoadingComplete) {
      this.setData({
        page: this.data.page + 1, //每次触发上拉事件，把searchPageNum+1
        isFirstSearch: false //触发到上拉事件，把isFromSearch设为为false
      });
      server.getJSON(common.basePath + 'speciality/list', {
        "page": this.data.page,
        "pageSize": 10,
        "status": this.data.currentTab
      }, res => {
        if (res.data.successful) {
          if (res.data.data.specialityList.length != 0) {
            let searchList = [];
            //如果isFromSearch是true从data中取出数据，否则先从原来的数据继续添加
            this.data.isFirstSearch ? searchList = res.data.data.specialityList : searchList = this.data.lists.concat(res.data.data.specialityList)
            this.setData({
              lists: searchList, //获取数据数组
              searchLoading: true //把"上拉加载"的变量设为false，显示
            });
            //没有数据了，把“没有数据”显示，把“上拉加载”隐藏
          } else {
            this.setData({
              searchLoadingComplete: true, //把“没有数据”设为true，显示
              searchLoading: false //把"上拉加载"的变量设为false，隐藏
            });
          }
        } else {
          common.showErrorToast(res.data.errmsg);
        }
      })
    }
  },

  goToDetail(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../../shop/pages/productDetail?goodsid=' + id,
    })
  },
  onPullDownRefresh() {
    this.setData({
      scrollnum: 0,
      page: 1,
      isFirstSearch: true,
      searchLoading: true, //"上拉加载"的变量，默认false，隐藏
      searchLoadingComplete: false, //“没有数据”的变量，默认false，隐藏
    })
    this.getLists(this.data.currentTab);
  },
  //分享事件
  onShareAppMessage(option) {
    var that = this;
    var shareObj = {
      path: '/pages/index/index',
      title: '好东西，手慢无'
    }
    return shareObj;
  },
  goToCart() {
    if (this.data.isLogin) {
      wx.navigateTo({
        url: '../../cart/pages/cartIndex',
      })
    } else {
      this.authLogin.showLoginPop()
    }
  },
  goToCoupon(){
    app.globalData.isClickRedBag=true;
    wx.navigateTo({
      url: '../../coupon/index',
    })
  },
  closePop(){
    app.globalData.isClickRedBag=true;
    this.setData({
      isClickRedBag:true
    })
  }
})