var common = require('../../utils/common');
var server = require('../../utils/server');
var promisify = require('../../utils/promisify');

var app = getApp();
Page({
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: '',
    hasUserInfo: false,
    logged: false,
    takeSession: false,
    requestResult: '',
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl'), // 如需尝试获取用户信息可改为false
    showLoginPop: false,
    canvasimg: '',
    isLogin: false
  },
  loginEvent() {
    this.data.isLogin = true;
    this.checkLogin();
    // this.getUserInfo()
  },
  regEvent() { },
  onLoad: function (options) {
    this.authLogin = this.selectComponent('#authLogin');
  },
  login() {
    this.authLogin.showLoginPop();
  },
  onShow: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#f90',
      animation: {
        duration: 400
      }
    })
    wx.setBackgroundColor({
      backgroundColor: '#ff9900', // 顶部窗口的背景色为
      backgroundColorBottom: '#ffffff' // 底部窗口的背景色为白色
    })
    this.checkLogin()
  },
  goToCoupon() {
    if (this.data.isLogin) {
      wx.navigateTo({
        url: '../../coupon/index',
      })
    } else {
      this.authLogin.showLoginPop()
    }

  },
  checkLogin() {
    const userInfo = wx.getStorageSync('token');
    common.checkToken(userInfo).then(res => {
      if (res.status != 1) {
        this.setData({
          isLogin: false,
        });
      } else {
        this.setData({
          isLogin: true,
        });
        this.getUserInfo()
      }
    })
  },
  getUserInfo() {
    server.getJSON(common.basePath + 'user/info', "", res => {
      this.setData({
        userInfo: res.data.data.user,
        qrcode: res.data.data.user.qrCode
      })
      wx.setStorage({
        key: 'userInfo',
        data: res.user
      })
    })
  },
  goToAddress() {
    if (this.data.isLogin) {
      wx.navigateTo({
        url: '../../address_manage/pages/addressIndex',
      })
    } else {
      this.authLogin.showLoginPop()
    }

  },
  callServer() {
    wx.previewImage({
      current: 'http://towe.oss-cn-shenzhen.aliyuncs.com/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20190215162614.jpg', // 当前显示图片的http链接
      urls: ['http://towe.oss-cn-shenzhen.aliyuncs.com/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20190215162614.jpg'] // 需要预览的图片http链接列表
    })
    // wx.makePhoneCall({
    //   phoneNumber: "18359205416"
    // })
  },
  recommendUser() {
    const wxGetImageInfo = promisify(wx.getImageInfo);
    var img = this.data.qrcode;
    console.log(img)
    Promise.all([

      wxGetImageInfo({
        src: img
      })
    ]).then(res => {
      console.log(res)
      const ctx = wx.createCanvasContext('shareCanvas')
      // 底图
      ctx.drawImage(res[0].path, 0, 0, 600, 900)
      // 作者名称
      ctx.setTextAlign('center') // 文字居中
      ctx.setFillStyle('#000000') // 文字颜色：黑色
      ctx.setFontSize(22) // 文字字号：22px
      ctx.fillText("元初食品", 250 / 2, 300)
      // 小程序码
      const qrImgSize = 100;
      ctx.drawImage(res[1].path, (250 - qrImgSize) / 2, 100, qrImgSize, qrImgSize);
      ctx.stroke();
      ctx.draw();
      const wxCanvasToTempFilePath = promisify(wx.canvasToTempFilePath)
      wxCanvasToTempFilePath({
        canvasId: 'shareCanvas'
      }, this).then(res => {
        console.log(res)
        this.setData({
          canvasimg: res.tempFilePath
        })
      })
    })

  }
})