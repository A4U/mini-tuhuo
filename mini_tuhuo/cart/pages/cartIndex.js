var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartList: [],
    isSelectAll: false,
    totalPrice: "",
    btnDisable: false,
    isSelectAll: true
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },
  onShow: function() {
    this.getCartList()
  },
  getCartList() {
    server.getJSON(common.basePath + 'cart/list', "", res => {
      if (res.data.successful) {
        this.setData({
          totalPrice: res.data.data.totalPrice
        })
        var cartList = res.data.data.cartList,
          selectedNum = 0;
        for (var j = 0; j < cartList.length; j++) {
          cartList[j].isCanAdd = true;
          cartList[j].isCanReduce = true;
          if (cartList[j].status) {
            selectedNum++;
          }
        }
        console.log(selectedNum, cartList.length)
        if (selectedNum == cartList.length) {
          this.data.isSelectAll = true;
        } else {
          this.data.isSelectAll = false;
        }
        if (selectedNum>0){
          this.data.btnDisable = false;
        }else{
          this.data.btnDisable = true;
        }
        this.setData({
          isSelectAll: this.data.isSelectAll,
          btnDisable: this.data.btnDisable,
          cartList: cartList
        })

      } else {
        common.showErrorToast(res.data.data.errmsg)
      }
    })
  },
  reduceGoodsNum(e) {
    var specialityId = e.currentTarget.dataset.goodsid;
    server.postJSON(common.basePath + 'cart/reduce?specialityId=' + specialityId, "", res => {
      if (res.data.successful) {
        this.getCartList();
      } else {
        common.showErrorToast(res.data.data.errmsg)

      }
    })
  },
  addGoodsNum(e) {
    var specialityId = e.currentTarget.dataset.goodsid;
    server.postJSON(common.basePath + 'cart/plus?specialityId=' + specialityId, "", res => {
      if (res.data.successful) {
        this.getCartList();
      } else {
        common.showErrorToast(res.data.data.errmsg)

      }
    })

  },
  selectSingle(e) {
    var status = e.currentTarget.dataset.selectstatus,
      selectType = e.currentTarget.dataset.selecttype,
      ids = [];
    if (selectType == 'all') {
      for (var i = 0; i < this.data.cartList.length; i++) {
        ids.push(this.data.cartList[i].id);
      }
    } else {
      ids.push(e.currentTarget.dataset.id)
    }
    console.log(parseInt(status))
    server.put(common.basePath + 'cart/pick', {
      "ids": ids,
      "status": parseInt(status)
    }, res => {
      if (res.data.successful) {
        this.getCartList();
      } else {
        common.showErrorToast(res.data.data.errmsg)
      }
    })
  },
  goToAccount() {
    var preOrderForms = [];
    for (var j = 0; j < this.data.cartList.length; j++) {
      if (this.data.cartList[j].status) {
        preOrderForms.push({
          "specialityId": this.data.cartList[j].specialityId,
          "count": this.data.cartList[j].num
        })
      }
    }
    server.postJSON(common.basePath + 'preOrder/save', {
      "preOrderForms": preOrderForms
    }, res => {
      if (res.data.successful) {
        wx.navigateTo({
          url: '../../order/pages/orderAccount?id=' + res.data.data.preOrderId,
        })
      } else {
        common.showErrorToast(res.data.data.errmsg)
      }
    })

  }
})