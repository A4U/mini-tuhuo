var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({
  data: {
    showDetailPop: false,
    addNum: 1,
    goodsName: "",
    goodsDetail: {},
    showLoginPop: false,
    isAddCart: "",
    totalNum: "",
    cartNum: 0,
    showHome:false
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getDetail(options.goodsid);
    this.setData({
      goodsId: options.goodsid
    })
  },
  onShow: function() {
    if (app.globalData.sceneId == 1007 || app.globalData.sceneId == 1008) {
      this.setData({
        showHome:true
      })
    }
    this.checkLogin();
    console.log(this.data.showDetailPop)
  },
  getDetail(goodsid) {
    server.getJSON(common.basePath + 'speciality/info/' + goodsid, "", res => {
      if (res.data.successful) {
        this.setData({
          goodsDetail: res.data.data.speciality,
          totalNum: res.data.data.speciality.inventory,
          addNum: res.data.data.speciality.cartNum == 0 ? 1 : res.data.data.speciality.cartNum
        })
      }

    })
  },
  getCartNum() {
    server.getJSON(common.basePath + 'cart', "", res => {
      if (res.data.successful) {
        console.log(res.data.data.cart)
        this.setData({
          cartNum: res.data.data.cart
        })
      } else {
        common.showErrorToast(res.data.errmsg);

      }
    })
  },
  checkLogin() {
    wx.login({
      success: resp => {
        console.log(resp)
        wx.getUserInfo({
          success: res => {
            this.setData({
              showLoginPop: false
            })
            console.log(res)
            var code = resp.code,
              userParam = {
                "gender": res.userInfo.gender,
                "avatar": res.userInfo.avatarUrl,
                "nickName": res.userInfo.nickName,
                "recommendId": app.globalData.friendId
              };
            wx.getStorage({
              key: 'token',
              success: res => {
                common.checkToken(res.data).then(res => {
                  if (res.status != 1) {
                    this.reLogin(userParam, code)
                  } else {
                    this.getCartNum();
                  }
                })
              },
              fail: res => {

              }
            })

          },
          fail: res => {
            this.setData({
              showLoginPop: true
            })
          }
        })
      }
    })
  },
  reLogin(userParam, code) {
    common.login(userParam, code).then(res => {
      wx.setStorage({
        key: 'token',
        data: res.user.token,
        success: res => {
          this.getCartNum();
        }
      })
    })
  },
  //登录
  login(e) {
    if (e.detail.errMsg == "getUserInfo:ok") {
      this.setData({
        showLoginPop: false
      })
      this.checkLogin();
    } else {
      wx.showToast({
        icon: 'none',
        title: '需要授权用户信息使用土货来了！'
      })
    }
  },
  showModel(e) {
    this.setData({
      isAddCart: e.currentTarget.dataset.type
    })
    console.log(wx.getStorageSync('token'))
    var token = wx.getStorageSync('token');
    if (token == 'undefined' || token == "") {
      this.setData({
        showLoginPop: true
      })
    } else {
      var animation = wx.createAnimation({
        duration: 200,
        timingFunction: "linear",
        delay: 0
      })
      this.animation = animation
      animation.translateY(300).step()
      this.setData({
        animationData: animation.export(),
        showDetailPop: !this.data.showDetailPop
      })
      setTimeout(function() {
        animation.translateY(0).step()
        this.setData({
          animationData: animation.export()
        })
      }.bind(this), 200)
    }

  },
  hideModel() {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showDetailPop: !this.data.showDetailPop
    })
    setTimeout(function() {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  reduceGoodsNum() {
    this.setData({
      addNum: parseInt(this.data.addNum) - 1
    })
  },
  addGoodsNum() {
    if (this.data.addNum < this.data.totalNum) {
      this.setData({
        addNum: parseInt(this.data.addNum) + 1
      })
    } else {
      wx.showToast({
        title: '下单数量超出库存啦~',
      })
    }
  },
  goToAccount() {
    console.log(this.data.isAddCart)
    if (this.data.isAddCart == 0) {

      var preOrderForms = [{
        "specialityId": this.data.goodsId,
        "count": this.data.addNum
      }]
      server.postJSON(common.basePath + 'preOrder/save', {
        "preOrderForms": preOrderForms
      }, res => {
        if (res.data.successful) {

          wx.navigateTo({
            url: '../../order/pages/orderAccount?id=' + res.data.data.preOrderId,
          })

        } else {
          common.showErrorToast(res.data.data.errmsg)
        }
      })
    } else {
      server.postJSON(common.basePath + 'cart/add', {
        "specialityId": this.data.goodsId,
        "number": this.data.addNum
      }, res => {
        if (res.data.successful) {
          this.hideModel();
          common.showToast("添加成功！");
          this.getCartNum();
        } else {
          common.showErrorToast(res.data.data.errmsg)
        }
      })
    }

  },
  //分享事件
  onShareAppMessage(option) {
    var that = this;
    var shareObj = {
      path: 'shop/pages/productDetail?goodsid=' + this.data.goodsId,
      title: this.data.goodsDetail.title
    }
    return shareObj;
  },
  goToCart() {
    wx.navigateTo({
      url: '../../cart/pages/cartIndex',
    })
  },
  backHome() {
    wx.switchTab({
      url: '../../pages/index/index',
    })
  }
})