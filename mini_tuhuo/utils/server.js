var common = require('common');
const app = getApp();
function __args() {
  var setting = {};
  if (arguments.length === 1 && typeof arguments[0] !== 'string'){
    setting = arguments[0];
  } else {
    setting.url = arguments[0];
    if (typeof arguments[1] === 'object') {
      setting.data = arguments[1];
      setting.callback = arguments[2];
    } else if (typeof arguments[1] === 'string') {//文件上传
      setting.filePath = arguments[1];
      setting.callback = arguments[2];
    } else {
      setting.callback = arguments[1];
    }
  }
  return setting;
}
function __loginjson(method, setting) {
  setting.method = method;
  setting.header = {
    'content-type': 'application/json'
  };
  setting.complete = function (ret) { onComplete(ret, setting.callback) };
  wx.request(setting);
}
function __json(method, setting,sso) {
  setting.method = method;
  console.log(app.globalData.token)
  setting.header = {
    'content-type': 'application/json',
    "token":wx.getStorageSync('token')
  };
  setting.complete = function (ret) { onComplete(ret, setting.callback) };
  wx.request(setting);
}

function __upload(setting) {
  setting.header = {
    "content-type": "multipart/form-data"
  };
  setting.name = 'file';
  setting.complete = function (ret) { onComplete(ret, setting.callback) };
  setting.fail = function (ret) {
  }
  wx.showToast({
    icon: "loading",
    title: "正在上传"
  });
  wx.uploadFile(setting);
}

function onComplete(ret, callback) {
  var errMsg = null;
  if (ret.statusCode == 400 || ret.statusCode == 401 || ret.statusCode == 500||ret.statusCode == 502) {
    errMsg = ret.data.message;
  } else if (ret.errMsg.indexOf(':fail') != -1) {
    errMsg = '网络不给力，请稍候再试';
  }
  if (errMsg) {
    wx.showModal({
      title: '提示信息',
      content: errMsg,
      showCancel: false
    })
  } else if (callback) {
    callback(ret);
  }
}

function __uploadOSS(setting, callback) {
  console.log(JSON.stringify(setting));
  wx.showToast({
    icon: "loading",
    title: "正在上传"
  });
  wx.uploadFile({
    url: setting.host,
    filePath: setting.filePath,
    name: 'file',
    formData: {
      'key': setting.key,
      'policy': setting.policy,
      'OSSAccessKeyId': setting.accessid,
      'success_action_status': '200', //让服务端返回200,不然，默认会返回204
      'signature': setting.signature,
    },
    success: function (res) {
      console.log(JSON.stringify(res))
      if (callback) {
        callback(res);
      }
    },
    fail: function (res) {
      console.log(JSON.stringify(res));
      common.showFail('上传失败');
    },
    complete: function (res) {
      wx.hideToast();
    }
  })
}

module.exports = {
  login: function () {
    __loginjson('POST', __args.apply(this, arguments));
  },
  getJSON: function () {
    __json('GET', __args.apply(this, arguments));
  },
  postJSON: function () {

    __json('POST', __args.apply(this, arguments));
  },
  del: function () {
    __json('DELETE', __args.apply(this, arguments));
  },
  put: function () {
    __json('PUT', __args.apply(this, arguments));
  },
  uploadFile: function () {
    __upload(__args.apply(this, arguments));
  },
  uploadOSS: function () {
    __uploadOSS(arguments[0], arguments[1]);
  },
  sendTemplate: function (formId, templateData, success, fail) {
    var app = getApp();
    this.postJSON({
      //url: '/WxAppApi/sendTemplate',
      url: 'http://localhost:8080/extrication/v1/picture/oss',
      header: {
        'content-type': 'application/json',
        'token': wx.getStorageSync('token'),
        'userId': wx.getStorageSync('userId')
      },
      data: {
        form_id: formId,
        data: templateData,
      },
      success: success,
      fail: fail
    });
  }
}