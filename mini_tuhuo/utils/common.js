module.exports = {
  basePath: 'https://www.it0592.cn/towe-api/', //测试环境
  // basePath: 'https://user.sungivenplus.com/api/',//生产环境
  gdKey: '553750cc24e9d426636f453ea3547e95',
  serverTel: '400-096-8066',
  login(userParam, code) {
    console.log(1)
    var server = require('server');
    var that = this;
    return new Promise(function(resolve, reject) {
      server.login(that.basePath + 'auth/login/' + code, userParam, ret => {
        if (ret.data.successful) {
          resolve(ret.data.data)
          console.log(ret)
        } else {
          that.showErrorToast(ret.data.data.errmsg)
        }
      })
    })
  },
  checkToken(token) {
    console.log(2)
    var server = require('server');
    var that = this;
    return new Promise(function (resolve, reject) {
      server.getJSON(that.basePath + '/auth/login/token?token='+token, "", ret => {
        if (ret.data.successful) {
          resolve(ret.data.data)
        } else {
          that.showErrorToast(ret.data.data.errmsg)
        }
      })
    })
  },
  getAddrList() {
    var server = require('server');
    var that = this;
    return new Promise(function(resolve, reject) {
      server.getJSON(that.basePath + 'userAddress/list', "", res => {
        if (res.data.successful) {
          resolve(res.data.data.addressList)
        } else {
          that.showErrorToast(res.data.data.errmsg)
        }
      })
    })

  },
  showFail: function(msg) {
    wx.showToast({
      image: '/imgs/icon/fail.png',
      title: msg
    })
  },
  showLoading: function(mesg) {
    wx.showLoading({
      title: mesg,
    })
  },
  hideLoading: function() {
    setTimeout(function() {
      wx.hideLoading()
    }, 500)
  },
  showToast: function(mesg) {
    wx.showToast({
      title: mesg,
      icon: 'none'
    })
  },
  showErrorToast: function(mesg) {
    if (mesg == undefined) {
      this.showToast("数据出错啦T_T，请稍候再试 [-3]");
    }
    wx.showToast({
      title: mesg,
      icon: 'none'
    })
  },
  hideToast: function() {
    setTimeout(function() {
      wx.hideToast()
    }, 3500)
  },
  call: function(e) {
    var tel = e.currentTarget.dataset.tel;
    wx.makePhoneCall({
      phoneNumber: tel
    })
  },
  //提交订单
  addOrder(param) {
    var server = require('server');
    var that = this;
    return new Promise(function(resolve, reject) {
      server.postJSON(that.basePath + 'order/save', param, res => {
        resolve(res.data)

      })
    })
  },
  payOrder(orderid) {
    var server = require('server');
    var that = this;
    return new Promise(function(resolve, reject) {
      server.getJSON(that.basePath + 'weChat/unifiedOrder/' + orderid, "", res => {
        wx.requestPayment({
          timeStamp: res.data.data.timeStamp,
          nonceStr: res.data.data.nonceStr,
          package: res.data.data.detailStr,
          signType: 'MD5',
          paySign: res.data.data.paySign,
          success:res=> {
            wx.switchTab({
              url: '../../pages/order/orderIndex',
            })
          },
          fail:res=>{
            wx.switchTab({
              url: '../../pages/order/orderIndex',
            })
          }
        })
      })
    })
  },
}