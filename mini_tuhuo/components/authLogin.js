// components/authLogin.js
const app = getApp();
const server = require('../utils/server');
const common = require('../utils/common');

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isNeedReg: { //0:当前页面不需要手机号，1:需要手机号
      type: String
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    showLoginPop: false,
    showPhonePop: false,
    isDisableLog: false,
    isDisableReg: false,
    canIUseGetUserProfile: true
  },
  lifetimes: {},
  /**
   * 组件的方法列表
   */
  methods: {
    showLoginPop() {
      this.setData({
        showLoginPop: true,
      });
      if (wx.getUserProfile) {
        this.setData({
          canIUseGetUserProfile: true
        })
      } else {
        this.setData({
          canIUseGetUserProfile: false
        })
      }
    },
    hideLoginPop() {
      this.setData({
        showLoginPop: false,
        isDisableLog: false
      })
    },
    showRegPop() {
      this.setData({
        showPhonePop: true,
      });
    },
    hidePhonePop() {
      this.setData({
        showPhonePop: false,
        isDisableReg: false
      })
    },
    // 判断是否登录
    isLogin(cb) {
      const userInfo = wx.getStorageSync('token');
      common.checkToken(userInfo).then(res => {
        if (res.status != 1) {
          this.setData({
            showLoginPop: true,
          });
          if (wx.getUserProfile) {
            this.setData({
              canIUseGetUserProfile: true
            })
          } else {
            this.setData({
              canIUseGetUserProfile: false
            })
          }
        } else {
          return typeof cb == "function" && cb();
        }
      })
      console.log("会员", userInfo)
    },
    // 判断是否登录
    isRegister(cb) {
      wx.getStorage({
        key: 'userInfo',
        success: res => {
          console.log(res);
          if (JSON.stringify(res.data.member) == '{}') {
            this.setData({
              showPhonePop: true,
            });
          } else {
            return typeof cb == "function" && cb();
          }
        }
      })
    },
    //新授权用户信息
    getUserProfile() {
      this.setData({
        isDisableLog: true
      })
      wx.getUserProfile({
        desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          // this.setData({
          //   userInfo: res.userInfo,
          //   avatar: res.userInfo.avatarUrl,
          //   nickname: res.userInfo.nickName,
          // })
          this.setData({
            isDisableLog: false
          })
          console.log("resdata", res);
          console.log(this.data.userInfo);
          this.getUserInfoNew(res);
        },
        fail: res => {
          this.setData({
            isDisableLog: false
          })
        }
      })
    },
    //新获取用户信息
    getUserInfoNew(res) {
      this.setData({
        isDisableLog: true
      })
      wx.login({
        success: resp => {
          var code = resp.code,
            that = this,
            userParam = {
              "gender": res.userInfo.gender,
              "avatar": res.userInfo.avatarUrl,
              "nickName": res.userInfo.nickName,
              "recommendId": app.globalData.friendId
            };
          common.login(userParam, code).then(res => {
            that.setData({
              showLoginPop: false,
              isDisableLog: false
            })
            that.storageUserinfo(res.user.token, 'islog')

          })
        },
        fail: res => {
          this.setData({
            isLogin: false,
            isDisableLog: false
          })
        }
      })
    },
    //低版本获取用户信息
    login(e) {
      this.setData({
        isDisableLog: true
      })
      if (e.detail.errMsg == "getUserInfo:ok") {
        this.setData({
          showLoginPop: false,
          isLogin: false
        })
        wx.login({
          success: resp => {
            wx.getUserInfo({
              success: res => {
                var code = resp.code,
                  that = this,
                  userParam = {
                    "gender": res.userInfo.gender,
                    "avatar": res.userInfo.avatarUrl,
                    "nickName": res.userInfo.nickName,
                    "recommendId": app.globalData.friendId
                  };
                common.login(userParam, code).then(res => {
             
                  that.setData({
                    showLoginPop: false,
                    isDisableLog: false
                  })
                  that.storageUserinfo(res.user.token, 'islog')

                })
              }
            })
          }
        })
      } else {
        this.setData({
          isDisableLog: false
        })
        wx.showToast({
          icon: 'none',
          title: '需要授权用户信息使用元初到家！'
        })
      }
    },
    //本地存储用户信息
    storageUserinfo(data, type) {
      wx.setStorage({
        key: "token",
        data: data
      })
      if (type == 'isreg') {
        this.triggerEvent('parentregevent')
      } else {
        this.triggerEvent('parentlogevent')
      }
      // this.checkSttVip()
    },
    showAgreement() {
      wx.navigateTo({
        url: '/pages/common/agreement',
      })
    },
    showCheckTel() {
      wx.navigateTo({
        url: '/pages/common/register',
      })
      this.setData({
        showPhonePop: false
      })
    },
    //授权手机号
    getPhoneNumber: function (e) {
      var that = this;
      this.setData({
        isDisableReg: true
      })
      if (e.detail.errMsg != "getPhoneNumber:ok") {
        wx.showToast({
          icon: 'none',
          title: '需要授权手机号后使用元初到家！',
        })
        this.setData({
          isDisableReg: false
        })
      } else {
        var that = this;
        wx.login({
          success: res => {
            this.setData({
              showPhonePop: false
            });
            server.postJSON(common.basePath + 'miniprogram/registermember/', {
              "iv": e.detail.iv,
              "encryptedData": e.detail.encryptedData,
              "js_code": res.code,
              "city": app.globalData.locateCity
            }, res => {
              if (res.data.errno == '0') {
                // that.setData({
                //   userInfo: res.data.data,
                //   isRegister: true,
                //   isDisableReg: false
                // })
                // that.getCartNum();
                // that.getOrderStatus();
                that.storageUserinfo(res.data.data, 'isreg')
              } else {
                this.setData({
                  isDisableReg: false
                })
                common.showErrorToast(res.data.errmsg);
              }
            })
          },
          fail: res => {
            this.setData({
              isDisableReg: false
            })
          }
        })
      }
    }
  }
})