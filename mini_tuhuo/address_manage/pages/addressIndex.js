var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({
  data: {
    addressList: []
  },
  onShow: function() {
    this.getAddressList();
  },
  getAddressList() {
    common.getAddrList().then(res => {
      this.setData({
        addressList: res
      })
    })
  },
  deleteAddress: function(e) {
    wx.showModal({
      title: '提示',
      content: '确认删除收货地址！',
      success: res => {
        if (res.confirm) {
          if (e.currentTarget.dataset.id == app.globalData.addrId) {
            common.showToast("无法删除当前选中的收货地址，请切换后再删除该地址");
            common.hideToast();
          } else {
            server.del(common.basePath + 'userAddress/' + e.currentTarget.dataset.id, "", res => {
              if (res.data.successful) {
                common.showToast("删除成功！");
                this.getAddressList();
              } else {
                common.showErrorToast(ret.data.data.errmsg)
              }
            })
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  setDefaultAddr(e) {
    console.log(e)
    var id = e.detail.value;
    server.put(common.basePath + 'userAddress/' + id, "", res => {
      if (res.data.successful) {
        common.showToast("设置成功！");
      }else{
        common.showErrorToast(res.data.data.errmsg);
      }
    })
  },
  goToEditAddr: function(e) {
    wx.navigateTo({
      url: 'editAddr?addrId=' + e.currentTarget.dataset.id + '&&addrType=0'
    })
  }
})