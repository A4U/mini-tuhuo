var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({
  data: {
    mobile: "",
    consignee: "",
    addrId: null,
    address: "",
    addrFromType: "",
    showCity: false,
    showProvince: false,
    showArea: false,
    animationData: [],
    provinceLists: [],
    cityLists: [],
    areaLists: [],
    curProvince: "",
    curProvinceId: "",
    curArea: "",
    curAreaId: "",
    curCity: "",
    curCityId: ""
  },
  onLoad: function(options = {}) {
    let addrId = options.addrId;
    this.setData({
      addrId: options.addrId,
      addrFromType: options.addrType
    })
    if (addrId != "undefined" && addrId != "") {
      wx.setNavigationBarTitle({
        title: '编辑地址'
      })
      server.getJSON(common.basePath + 'userAddress/info/' + addrId, "", res => {
        if (res.data.successful) {
          this.setData({
            curProvince: res.data.data.userAddress.province,
            curProvinceId: res.data.data.userAddress.provinceId,
            curArea: res.data.data.userAddress.area,
            curAreaId: res.data.data.userAddress.areaId,
            curCity: res.data.data.userAddress.city,
            curCityId: res.data.data.userAddress.cityId,
            mobile: res.data.data.userAddress.contactMobile,
            consignee: res.data.data.userAddress.receiver,
            address: res.data.data.userAddress.address
          })
        } else {
          common.showErrorToast(ret.data.data.errmsg)
        }
      })
    } else {
      wx.setNavigationBarTitle({
        title: '新建地址'
      })
    }
  },
  formSubmit: function(e) {
    var testTel = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
    if (e.detail.value.mobile == "" || !(testTel.test(e.detail.value.mobile))) {
      common.showToast("请输入正确的手机号格式")
    } else if (e.detail.value.consignee == "" || e.detail.value.consignee.length < 2) {
      common.showToast("请输入收货人姓名")
    } else if (e.detail.value.address == "") {
      common.showToast("请输入详细地址")
    } else if (e.detail.value.area == "") {
      common.showToast("请选择地区")
    } else {
      console.log(this.data.addrId)
      var editParam = {};
      if (this.data.addrId != "undefined" && this.data.addrId != "") {
        console.log(223)
        editParam = {
          "receiver": e.detail.value.consignee,
          "contactMobile": e.detail.value.mobile,
          "provinceId": this.data.curProvinceId,
          "cityId": this.data.curCityId,
          "areaId": this.data.curAreaId,
          "address": e.detail.value.address,
          "id": this.data.addrId
        }
        server.put(common.basePath + 'userAddress/update', editParam, res => {
          if (res.data.successful) {
            common.showToast("添加成功！")
            wx.navigateBack();
          } else {
            common.showErrorToast(ret.data.data.errmsg)
          }
        })
      } else {
        editParam = {
          "receiver": e.detail.value.consignee,
          "contactMobile": e.detail.value.mobile,
          "provinceId": this.data.curProvinceId,
          "cityId": this.data.curCityId,
          "areaId": this.data.curAreaId,
          "address": e.detail.value.address,
        }
        server.postJSON(common.basePath + 'userAddress/save', editParam, res => {
          if (res.data.successful) {
            common.showToast("添加成功！")
            wx.navigateBack();
          } else {
            common.showErrorToast(ret.data.data.errmsg)
          }
        })
      }
    }
  },
  animate() {
    var animation = wx.createAnimation({
      duration: 300,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showAddrStatus: !this.data.showAddrStatus
    })
    setTimeout(function() {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  // 显示遮罩层
  getNewAddr: function() {
    this.animate()
  },
  // 隐藏遮罩层
  hideModal: function() {
    this.animate()
  },
  //选择地址标签
  chooseAddrLa(e) {
    this.setData({
      laSeleted: e.currentTarget.dataset.id
    })
  },
  showCityAnimate(type) {
    var animation = wx.createAnimation({
      duration: 300,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    if (type == "PROVINCE") {
      this.setData({
        animationData: animation.export(),
        showProvince: !this.data.showProvince
      })
    } else if (type == "CITY") {
      this.setData({
        animationData: animation.export(),
        showCity: !this.data.showCity
      })
    } else if (type == "AREA") {
      this.setData({
        animationData: animation.export(),
        showArea: !this.data.showArea
      })
    }
    setTimeout(function() {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  showCityList(e) {
    var type = e.currentTarget.dataset.type;
    if (type == "PROVINCE") {
      server.getJSON(common.basePath + 'basicCity/list', {
        "areaType": type,
        "parentId": ""
      }, res => {
        this.setData({
          provinceLists: res.data.data.areaList
        })
      })
    } else if (type == "CITY") {
      server.getJSON(common.basePath + 'basicCity/list', {
        "areaType": type,
        "parentId": this.data.curProvinceId
      }, res => {
        this.setData({
          cityLists: res.data.data.areaList
        })
      })
    } else if (type == "AREA") {
      server.getJSON(common.basePath + 'basicCity/list', {
        "areaType": type,
        "parentId": this.data.curCityId
      }, res => {
        this.setData({
          areaLists: res.data.data.areaList
        })
      })
    }

    this.showCityAnimate(type);

  },
  hideCityModal(e) {
    var type = e.currentTarget.dataset.type;
    this.showCityAnimate(type);

  },
  selectProvince(e) {
    var type = e.currentTarget.dataset.type;
    if (this.data.curProvince != e.currentTarget.dataset.name) {
      this.setData({
        curArea: "",
        curAreaId: "",
        curCity: "",
        curCityId: ""
      })
    }
    this.setData({
      curProvince: e.currentTarget.dataset.name,
      curProvinceId: e.currentTarget.dataset.provinceid
    })
    this.showCityAnimate(type);
    this.showCityAnimate("CITY");
    server.getJSON(common.basePath + 'basicCity/list', {
      "areaType": "CITY",
      "parentId": this.data.curProvinceId
    }, res => {
      this.setData({
        cityLists: res.data.data.areaList
      })
    })
  },
  selectCity(e) {
    var type = e.currentTarget.dataset.type;
    if (this.data.curCity != e.currentTarget.dataset.name) {
      this.setData({
        curArea: "",
        curAreaId: ""
      })
    }
    this.setData({
      curCity: e.currentTarget.dataset.name,
      curCityId: e.currentTarget.dataset.cityid
    })
    this.showCityAnimate(type);
    this.showCityAnimate("AREA");
    server.getJSON(common.basePath + 'basicCity/list', {
      "areaType": "AREA",
      "parentId": this.data.curCityId
    }, res => {
      this.setData({
        areaLists: res.data.data.areaList
      })
    })
  },
  selectArea(e) {
    var type = e.currentTarget.dataset.type;
    this.setData({
      curArea: e.currentTarget.dataset.name,
      curAreaId: e.currentTarget.dataset.areaid
    })
    this.showCityAnimate(type);
  }
})