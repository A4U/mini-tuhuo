var server = require('../../utils/server');
var common = require('../../utils/common');
var app = getApp();
Page({
  data: {
    deliveryFee: 0,
    addressLists: [],
    defaultAddr: [],
    showAddrStatus: false,
    smsStatus: false,
    isPayOk: false
  },
  onLoad: function(options) {
    this.setData({
      preOrderId: options.id
    })

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'token',
      success: res => {
        common.checkToken(res.data).then(res => {
          if (res.status == 1) {
            server.getJSON(common.basePath + 'preOrder/info/' + this.data.preOrderId, "", res => {
              if (res.data.successful) {
                var data = res.data.data;
                this.setData({
                  orderInfo: data.preOrder,
                  postageLimit: data.deliverLimit,
                  totalCount: data.totalCount,
                  totalPrice: data.totalPrice
                })
                common.getAddrList().then(resp => {
                  if(res.data.successful){
                    let length = resp.length;
                    if (length > 0) {
                      this.setData({
                        addressLists: resp
                      })
                      var defaultAddr = resp.filter(function (item, index) {
                        return item.status == 1
                      })
                      console.log(defaultAddr)
                      this.setData({
                        defaultAddr: defaultAddr[0],
                        deliveryFee: this.data.totalCount >= this.data.postageLimit ? 0 : defaultAddr[0].deliverPrice
                      })
                    } else {
                      this.setData({
                        deliveryFee: 0
                      })
                    }
                  }else{
                    common.showErrorToast(res.data.errmsg)
                  }
                })
              } else {
                common.showErrorToast(res.data.errmsg)
              }
            })
          } else {
            common.showErrorToast("请先登录！");
            wx.switchTab({
              url: '../../pages/index/index',
            })
          }
        })
      },
      fail: res => {
        wx.switchTab({
          url: '../../pages/index/index',
        })
      }
    })
  },

  showModal() {
    var animation = wx.createAnimation({
      duration: 300,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showAddrStatus: !this.data.showAddrStatus
    })
    setTimeout(function() {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  sendMesg(e) {
    console.log(e)
    this.setData({
      smsStatus: e.detail.value ? 1 : 0
    })
  },
  goToAddr(e) {
    wx.navigateTo({
      url: '../../address_manage/pages/editAddr?addrId=' + e.currentTarget.dataset.id + '&&addrType=1'
    })
  },
  chooseNewAddr(e) {
    console.log(e)
    this.setData({
      defaultAddr: e.currentTarget.dataset.item,
      deliveryFee: this.data.totalCount >= this.data.postageLimit ? 0 : e.currentTarget.dataset.item.deliverPrice
    })
    console.log(this.data.defaultAddr)
    this.showModal()
  },
  getRemark(e) {
    this.setData({
      remark: e.detail.value
    })
  },
  addOrder() {
    console.log(this.data.defaultAddr.id)
    if (this.data.defaultAddr.id == undefined) {
      common.showErrorToast("请先完善收货地址！")
    } else {
      this.setData({
        isPayOk: true
      })
      var orderParam = {
        "preOrderId": this.data.preOrderId,
        "smsStatus": this.data.smsStatus,
        "deliverPrice": this.data.defaultAddr.deliverPrice,
        "receiverId": this.data.defaultAddr.id,
        "remark": this.data.remark
      }
      common.addOrder(orderParam).then(res => {
        console.log(res)
        if (res.successful) {
          common.payOrder(res.data.orderNo)
        } else {
          common.showErrorToast(res.data.errmsg)
        }
      })
    }

  }
})