var server = require('../../utils/server');
var common = require('../../utils/common');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.data.orderId = options.orderid
    this.getDetail();
  },
  getDetail() {
    server.getJSON(common.basePath + 'order/info/' + this.data.orderId, "", res => {
      if (res.data.successful) {
        this.setData({
          orderInfo: res.data.data.order
        })
      } else {
        common.showErrorToast(res.data.data.errmsg)
      }
    })
  },
  goToDetail(e) {
    wx.navigateTo({
      url: '../../shop/pages/productDetail?goodsid=' + e.currentTarget.dataset.goodsid,
    })
  }

})