//app.js
App({
  onLaunch: function() {

  },
  onShow: function(options) {
    this.globalData.sceneId = options.scene;
  },
  globalData: {
    showLoginPop: false,
    userInfo: null,
    token: null,
    friendId: null,
    sceneId: null,
    isClickRedBag:false
  }
})